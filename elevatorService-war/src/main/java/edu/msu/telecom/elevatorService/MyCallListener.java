/*
    Copyright 2014 Michigan State University, Board of Trustees

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.

 */

package edu.msu.telecom.elevatorService;

import com.avaya.collaboration.businessdata.api.NoAttributeFoundException;
import com.avaya.collaboration.businessdata.api.ServiceData;
import com.avaya.collaboration.businessdata.api.ServiceNotFoundException;
import com.avaya.collaboration.call.Call;
import com.avaya.collaboration.call.CallListenerAbstract;
import com.avaya.collaboration.call.Participant;
import com.avaya.collaboration.call.TheCallListener;
import com.avaya.collaboration.dal.factory.CollaborationDataFactory;
import com.avaya.collaboration.util.logger.Logger;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import com.twilio.sdk.TwilioRestClient;
import com.twilio.sdk.TwilioRestException;
import com.twilio.sdk.resource.factory.SmsFactory;
import java.util.HashMap;
import java.util.Map;

@TheCallListener
public class MyCallListener extends CallListenerAbstract 
{
	private Connection connect = null;
	private Statement statement = null;
	private ResultSet resultSet = null;

	private static Logger logger = Logger.getLogger(MyCallListener.class);
   
	public MyCallListener() 
	{
	}
	
	@Override
	public final void callIntercepted(final Call call) 
	{
        ServiceData svcData = CollaborationDataFactory.getServiceData("elevatorService", "1.2.0.0.0");
        String mysqlHost = "";
        String mysqlUser = "";
        String mysqlPass = "";
        String mysqlDB = "";
        String twilioAccount = "";
        String twilioToken = "";
        String smsNumber = "";
        
		try 
		{
			mysqlHost = svcData.getGlobalServiceAttribute("sqlServer");
			mysqlUser = svcData.getGlobalServiceAttribute("sqlLogin");
			mysqlPass = svcData.getGlobalServiceEncryptedAttribute("sqlPassword");
			mysqlDB = svcData.getGlobalServiceAttribute("sqlDB");
			twilioAccount = svcData.getGlobalServiceAttribute("twilioLogin");
			twilioToken = svcData.getGlobalServiceEncryptedAttribute("twilioToken");
			smsNumber = svcData.getGlobalServiceAttribute("smsNumber");
		} 
		catch (NoAttributeFoundException e) 
		{
			logger.fatal("Unable to pull attributes from System Manager... DB Lookup fails.");
		}
		catch (ServiceNotFoundException e) 
		{
			logger.fatal("Service Attributes were not found. DB Lookup fails.");
		}
        
        String phoneNumber = getCallerPhoneNumber(call);
        ElevatorLocation elevator = getElevatorLocationInfo(phoneNumber, mysqlHost, mysqlUser, mysqlPass, mysqlDB);
        
        setCallerInformation(call, elevator);
        writeLog(call, mysqlHost, mysqlUser, mysqlPass, mysqlDB);
        String onCallPerson = getOnCallPerson(elevator, mysqlHost, mysqlUser, mysqlPass, mysqlDB);
        
        sendSMS(elevator, twilioAccount, twilioToken, onCallPerson, smsNumber);
        
        call.allow();
	}

	private void setCallerInformation(Call call, ElevatorLocation elevatorInfo) 
	{
		call.getCallingParty().setPresentedDisplayName("!!" + elevatorInfo.getCallerID());
	}

	private String getCallerPhoneNumber(Call call) 
	{
		Participant participant = call.getCallingParty();
		return participant.getHandle();
	}
	
	private void sendSMS(ElevatorLocation elevatorInfo, String ACCOUNT_SID, String AUTH_TOKEN, 
			String numberTo, String numberFrom)
	{		
		TwilioRestClient client = new TwilioRestClient(ACCOUNT_SID, AUTH_TOKEN);
	   
		Map<String, String> params = new HashMap<String, String>();
		params.put("Body", "Elevator Alert! " + elevatorInfo.getLongName());
		params.put("To", numberTo);
		params.put("From", numberFrom);
		 
		    SmsFactory messageFactory = client.getAccount().getSmsFactory();
			try 
			{
				messageFactory.create(params);
			} 
			catch (TwilioRestException e) 
			{
				// Just eat the error.  If Twilio Fails, we don't care that much
		}
	}
	
	public void writeLog(Call call, String mySQLHost, String mySQLUser,
			String mySQLPass, String mySQLDB)
	{
		
		try
		{
	      // This will load the MySQL driver, each DB has its own driver
	      Class.forName("com.mysql.jdbc.Driver");
	      // Setup the connection with the DB
	      connect = DriverManager
		          .getConnection("jdbc:mysql://" + mySQLHost + ":3306/" + mySQLDB + "?"
		              + "user=" + mySQLUser + "&password=" + mySQLPass);

	      // Statements allow to issue SQL queries to the database
	      statement = connect.createStatement();
	      // Result set get the result of the SQL query
	      statement.executeUpdate("INSERT INTO elevatorlog (logDate,locationID) VALUES(NOW(),'" + call.getCallingParty().getHandle() + "')");
	       return;			
		}
		catch (Exception e)
		{
			//throw new RuntimeException("problem connecting to database");
			logger.fatal("There was a problem writing to the log database. " + e.toString());
		}
		finally
		{
			close();
		}
	}
	
	public ElevatorLocation getElevatorLocationInfo(String callID, String mySQLHost, String mySQLUser,
			String mySQLPass, String mySQLDB) 
	{
	    try 
	    {
	      // This will load the MySQL driver, each DB has its own driver
	      Class.forName("com.mysql.jdbc.Driver");
	      // Setup the connection with the DB
	      connect = DriverManager
	          .getConnection("jdbc:mysql://" + mySQLHost + ":3306/" + mySQLDB + "?"
	              + "user=" + mySQLUser + "&password=" + mySQLPass);

	      // Statements allow to issue SQL queries to the database
	      statement = connect.createStatement();
	      // Result set get the result of the SQL query
	      resultSet = statement
	          .executeQuery("SELECT * FROM elevatorLocations WHERE phoneNumber = " + callID);
	       return writeElevatorObject(resultSet);
	      
	    } 
	    catch (Exception e) 
	    {
	      //throw new RuntimeException("problem connecting to database");
	      logger.fatal("There was an error reading from the DB - " + e.toString());
	    } 
	    finally 
	    {
	      close();
	    }
	    return new ElevatorLocation();
	  }

	public String getOnCallPerson(ElevatorLocation elevator, String mySQLHost, String mySQLUser,
			String mySQLPass, String mySQLDB) 
	{
	    try 
	    {
	      // This will load the MySQL driver, each DB has its own driver
	      Class.forName("com.mysql.jdbc.Driver");
	      // Setup the connection with the DB
	      connect = DriverManager
	          .getConnection("jdbc:mysql://" + mySQLHost + ":3306/" + mySQLDB + "?"
	              + "user=" + mySQLUser + "&password=" + mySQLPass);

	      // Statements allow to issue SQL queries to the database
	      statement = connect.createStatement();
	      // Result set get the result of the SQL query
	      resultSet = statement
	          .executeQuery("SELECT onCallCell FROM buildingSchedule WHERE buildingID = " + elevator.getBuildingNumber() + " AND now() BETWEEN shiftStart AND shiftEnd");
	       return getOnCallNumber(resultSet);
	      
	    } 
	    catch (Exception e) 
	    {
	      //throw new RuntimeException("problem connecting to database");
	      logger.fatal("There was an error reading from the DB - " + e.toString());
	    } 
	    finally 
	    {
	      close();
	    }
	    return "";
	  }
	
	
	  private ElevatorLocation writeElevatorObject(ResultSet resultSet) throws SQLException 
	  {
		    // ResultSet is initially before the first data set
		    if (resultSet.next()) 
		    {

		      ElevatorLocation myElevator = new ElevatorLocation();
		      myElevator.setCallerID(resultSet.getString("callerID"));
		      myElevator.setLocationID(resultSet.getInt("locationID"));
		      myElevator.setLongName(resultSet.getString("longName"));
		      myElevator.setPhoneNumber(resultSet.getString("phoneNumber"));
		      myElevator.setBuildingNumber(resultSet.getInt("buildingNumber"));
		      		    	
		      return myElevator;
		    }
		    return null;
		  }

	  // You need to close the resultSet
	  private void close() 
	  {
	    try 
	    {
	      if (resultSet != null) 
	      {
	        resultSet.close();
	      }

	      if (statement != null) 
	      {
	        statement.close();
	      }

	      if (connect != null) 
	      {
	        connect.close();
	      }
	    } 
	    catch (Exception e) 
	    {
	    	//do nothing.  
	    }
	  }

	  private String getOnCallNumber(ResultSet resultSet) throws SQLException 
	  {
		    // ResultSet is initially before the first data set
		    if (resultSet.next()) 
		    {
		      String myNumber = resultSet.getString("onCallCell");		      		    	
		      return myNumber;
		    }
		    return null;
		  }

}
