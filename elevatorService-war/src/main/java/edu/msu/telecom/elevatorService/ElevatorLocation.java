/*
    Copyright 2014 Michigan State University, Board of Trustees

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.

 */

package edu.msu.telecom.elevatorService;

public class ElevatorLocation 
{

//	SELECT
//	`elevatorLocations`.`locationID`,
//	`elevatorLocations`.`phoneNumber`,
//	`elevatorLocations`.`callerID`,
//	`elevatorLocations`.`longName`,
//  `elevatorLocations`.`buildingNumber`
//	FROM `elevator`.`elevatorLocations`;	

	private int locationID;
	private String phoneNumber;
	private String callerID;
	private String longName;
	private int buildingNumber;

	/**
	 * @return the locationID
	 */
	public int getLocationID() 
	{
		return locationID;
	}
	
	/**
	 * @param locationID the locationID to set
	 */
	public void setLocationID(int locationID) 
	{
		this.locationID = locationID;
	}

	/**
	 * @return the phoneNumber
	 */
	public String getPhoneNumber() 
	{
		return phoneNumber;
	}

	/**
	 * @param phoneNumber the phoneNumber to set
	 */
	public void setPhoneNumber(String phoneNumber) 
	{
		this.phoneNumber = phoneNumber;
	}

	/**
	 * @return the callerID
	 */
	public String getCallerID() 
	{
		return callerID;
	}

	/**
	 * @param callerID the callerID to set
	 */
	public void setCallerID(String callerID) 
	{
		this.callerID = callerID;
	}

	/**
	 * @return the longName
	 */
	public String getLongName() 
	{
		return longName;
	}

	/**
	 * @param longName the longName to set
	 */
	public void setLongName(String longName) 
	{
		this.longName = longName;
	}

	/**
	 * @return the buildingNumber
	 */
	public int getBuildingNumber() {
		return buildingNumber;
	}

	/**
	 * @param buildingNumber the buildingNumber to set
	 */
	public void setBuildingNumber(int buildingNumber) {
		this.buildingNumber = buildingNumber;
	}
	
}
